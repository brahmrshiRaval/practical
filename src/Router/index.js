import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Login from "../Components/Login";
import Service from "../Components/Services";
import Register from "../Components/Register";
// import { getSession } from "../utils/Session";

// const PrivateRoute = ({ component: Component, ...rest }) => {
//   const user = getSession("token");
//   return (
//     <Route
//       {...rest}
//       render={(props) =>
//         user ? (
//           <Component user={user} {...props} />
//         ) : (
//           <Route exact path="/" component={Login} />
//         )
//       }
//     />
//   );
// };
export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
    };
  }
  render() {
    return (
      <Router>
        <div>
          <Switch>
            <Route path="/service" component={Service} />
            <Route exact path="/" component={Login} />
            <Route exact path="/register" component={Register} />
          </Switch>
        </div>
      </Router>
    );
  }
}
