export function getParamsFromQuery(location,key){
    return new URLSearchParams(location).get(key);
}