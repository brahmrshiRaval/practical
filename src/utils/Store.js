import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";
import rootReducer from "../reducer";
import {
  persistStore,
  // persistCombineReducers,
  persistReducer,
} from "redux-persist";
import storage from "redux-persist/lib/storage";
export default function configureStore(initialState) {
  const reducer = combineReducers({
    auth: () => ({ mock: true }),
    form: persistReducer(
      {
        key: "form", // key for localStorage key, will be: "persist:form"
        storage,
        debug: true,
        blacklist: ["foo"],
      },
      rootReducer
    ),
  });
  const store = createStore(
    persistReducer(
      {
        key: "root",
        debug: true,
        storage,
        whitelist: ["auth"],
      },
      reducer
    ),
    initialState,
    applyMiddleware(thunk)
  );

  console.log("initialState", store.getState());
  const persistor = persistStore(store, null, () => {
    // if you want to get restoredState
  });

  return { store, persistor };
  // return createStore(rootReducer, initialState, applyMiddleware(thunk));
}
