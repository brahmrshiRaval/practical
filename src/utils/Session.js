import SecureStorage from "secure-web-storage"
import CryptoJS from "crypto-js"
 
const SECRET_KEY = 'kdJKIghsbdf_4FDSazd2g';
const SESSION_NAME = "_jkbgHD75fdsjkbn"
 
var secureStorage = new SecureStorage(localStorage, {
    hash: function hash(key) {
        key = CryptoJS.SHA256(key, SECRET_KEY);
        return key.toString();
    },
    encrypt: function encrypt(data) {
        data = CryptoJS.AES.encrypt(data, SECRET_KEY);
        data = data.toString();
        return data;
    },
    decrypt: function decrypt(data) {
        data = CryptoJS.AES.decrypt(data, SECRET_KEY);
        data = data.toString(CryptoJS.enc.Utf8);
        return data;
    }
});

export const setSession = (data) => {
    debugger
    secureStorage.setItem(SESSION_NAME,data)
}

export const getSession = (key) => {
    const session = secureStorage.getItem(SESSION_NAME);
    return session ? key ? session[key] : session : null;
}

export const removeSession = () => {
    secureStorage.removeItem(SESSION_NAME)
}

// secureStorage.key(id)
// // returns the hashed version of the key you passed into setItem with the given id.
 
// secureStorage.clear();
// // clears all data in the underlining sessionStorage/localStorage.
 
// secureStorage.length;
// // the number of entries in the underlining sessionStorage/localStorage.