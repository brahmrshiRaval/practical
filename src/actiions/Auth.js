import {
  FETCH_ERROR,
  FETCH_START,
  FETCH_SUCCESS,
  SIGNOUT_USER_SUCCESS,
  USER_DATA,
  USER_TOKEN_SET,
} from "./constant";
import { removeSession } from "../utils/Session";

export const userSignIn = (value) => {
  debugger;
  return (dispatch) => {
    dispatch({ type: FETCH_START });
    debugger;
    // if (value.email === "demo@gmail.com" && value.password === "demo") {
      // setSession(value);
      dispatch({ type: FETCH_SUCCESS });
      dispatch({ type: USER_TOKEN_SET });
      dispatch({ type: USER_DATA, value });
    // } else {
    //   dispatch({ type: FETCH_ERROR, payload: value });
    // }
  };
};
export const userSignUp = (value) => {
  console.log(value);
  return (dispatch) => {
    debugger;
    dispatch({ type: "USER_SIGN_UP", payload: value });
  };
};

export const userSignOut = () => {
  return (dispatch) => {
    dispatch({ type: FETCH_START });
    dispatch({ type: FETCH_SUCCESS });
    dispatch({ type: "USER_SIGN_OUT" });
    dispatch({ type: "CLEAR_CART" });
    removeSession();
    dispatch({ type: FETCH_SUCCESS });
    dispatch({ type: SIGNOUT_USER_SUCCESS });
  };
};
