import {
  INIT_URL,
  SIGNOUT_USER_SUCCESS,
  USER_DATA,
  USER_TOKEN_SET,
} from "../actiions/constant";
import { getSession } from "../utils/Session";

const INIT_STATE = {
  token: false,
  initURL: "",
  userData: [],
  authUser: getSession("user"),
  isOTPVerify: false,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case INIT_URL: {
      return { ...state, initURL: action.payload };
    }

    case SIGNOUT_USER_SUCCESS: {
      return {
        ...state,
        token: null,
        authUser: null,
        initURL: "",
      };
    }
    case USER_DATA: {
      return {
        ...state,
        authUser: action.payload,
      };
    }

    case USER_TOKEN_SET: {
      debugger;
      return {
        ...state,
        token: true,
        isOTPVerify: true,
      };
    }
    case "USER_OTP_VERIFY": {
      debugger;
      return {
        ...state,
        isOTPVerify: true,
      };
    }
    case "USER_SIGN_OUT": {
      debugger;
      return {
        ...state,
        isOTPVerify: false,
        token: false,
        authUser: null,
      };
    }
    case "USER_SIGN_UP": {
      debugger;
      return {
        ...state,
        token: true,
        isOTPVerify: true,
        userData: action.payload.array,
      };
    }

    default:
      return state;
  }
};
