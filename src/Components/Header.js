import React, { Component } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  Container,
  Row,
  Col,
} from "reactstrap";
import { Tooltip, Button } from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import { userSignOut } from "../actiions/index";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import { removeSession } from "../utils/Session";

class Navigaion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }
  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  };
  logout = () => {
    debugger;
    removeSession();
    this.props.signOut();
  };
  render() {
    const token = this.props.token;
    console.log(token);
    return (
      <div className="header">
        <Navbar expand="md">
          <Container className="containar-margin">
            <Row className="mobile-logo-menu">
              <NavbarBrand>
                <NavLink to="/">
                  <h1>Logo</h1>
                </NavLink>
              </NavbarBrand>
              <NavbarToggler onClick={this.toggle} className="menutoggle">
                <MenuIcon />
              </NavbarToggler>
            </Row>
            <div>
              <Collapse
                isOpen={this.state.isOpen}
                navbar
                className="justify-content-flex-end"
              >
                <Row className="upper-menu desktop">
                  <Col className="pr-2">
                    {token === true ? (
                      <NavItem className="list-style-button">
                        <NavLink to="/" onClick={this.logout}>
                          <div className="login-button">LogOut</div>
                        </NavLink>
                      </NavItem>
                    ) : (
                      <NavItem style={{ listStyleType: "none" }}>
                        <NavLink to="/login">
                          <div className="login-button">LogIn</div>
                        </NavLink>
                      </NavItem>
                    )}
                  </Col>
                  <Col className="pl-2 pr-2">
                    {this.props.itemCount === 0 ? (
                      <Tooltip title="Oops It's Empty">
                        <NavItem className="list-style-button">
                          <div className="Cart-button">
                            <Button variant="outlined" color="secondary">
                              {/* <NavLink to="/cart"> */}
                              Cart ({this.props.itemCount}){/* </NavLink> */}
                            </Button>
                          </div>
                        </NavItem>
                      </Tooltip>
                    ) : (
                      <Tooltip title={`Items in cart ${this.props.itemCount}`}>
                        <NavItem className="list-style-button">
                          <div className="Cart-button">
                            <Button variant="outlined" color="secondary">
                              <NavLink to="/cart">
                                Cart ({this.props.itemCount})
                              </NavLink>
                            </Button>
                          </div>
                        </NavItem>
                      </Tooltip>
                    )}
                  </Col>
                </Row>
                <div className="mobile ml-10 ">
                  <Row className="upper-menu  d-flex mt-30">
                    {token === true ? (
                      <Col>
                        <NavItem
                          className="list-style-button mr-10"
                          onClick={this.toggle}
                        >
                          <NavLink to="/" onClick={this.logout}>
                            <div className="login-button">LogOut</div>
                          </NavLink>
                        </NavItem>
                      </Col>
                    ) : (
                      <Col>
                        <NavItem
                          style={{ listStyleType: "none" }}
                          className="mr-10 list-style-button"
                          onClick={this.toggle}
                        >
                          <NavLink to="/login">
                            <div className="login-button">LogIn</div>
                          </NavLink>
                        </NavItem>
                      </Col>
                    )}
                    <Col>
                      {this.props.itemCount === 0 ? (
                        <Tooltip title="Oops It's Empty">
                          <NavItem className="list-style-button">
                            <div className="Cart-button">
                              <Button variant="outlined" color="secondary">
                                {/* <NavLink to="/cart"> */}
                                Cart ({this.props.itemCount}){/* </NavLink> */}
                              </Button>
                            </div>
                          </NavItem>
                        </Tooltip>
                      ) : (
                        <Tooltip
                          title={`Items in cart ${this.props.itemCount}`}
                        >
                          <NavItem className="list-style-button">
                            <div className="Cart-button">
                              <Button variant="outlined" color="secondary">
                                <NavLink to="/cart">
                                  Cart ({this.props.itemCount})
                                </NavLink>
                              </Button>
                            </div>
                          </NavItem>
                        </Tooltip>
                      )}
                    </Col>
                  </Row>
                </div>
              </Collapse>
            </div>
          </Container>
        </Navbar>
      </div>
    );
  }
}
const mapStateToProps = (state, response) => {
  debugger;
  return {
    token:
      state.form.Auth.authUser === null ? false : state.form.Auth.isOTPVerify,
    itemCount: state.form.Cart?.cart?.length || 0,
  };
};
const mapDispatchToProps = (dispatch) => {
  debugger;
  return {
    // dispatching plain actions
    userSignOut: userSignOut,
    signOut: () => dispatch({ type: "USER_SIGN_OUT" }),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Navigaion);
