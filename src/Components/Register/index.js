import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { userSignUp } from "../../actiions/Auth";
import { Formik } from "formik";
import * as Yup from "yup";
const SignUp = (props) => {
  const [name] = useState("");
  const [email] = useState("");
  const [password] = useState("");
  const dispatch = useDispatch();
  debugger;
  const user = useSelector((Auth) => Auth.form.Auth.userData);
  const token = useSelector((Auth) => Auth.form.Auth.token);
  useEffect(() => {
    debugger;
    if (token) {
      console.log(props.history);
      props.history.push("/service");
    }
  }, [token, props.history]);
  const handleClickSubmit = (values) => {
    debugger;
    let userData = {
      name: values.name,
      email: values.email,
      password: values.password,
    };

    let array = user === undefined ? [] : user;
    array.push(userData);
    dispatch(userSignUp({ array }));
  };
  const handleRegister = () => {
    props.history.push("/");
  };
  return (
    <div className="app-login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
      <div className="app-login-main-content">
        <div className="app-login-content">
          <div className="app-login-header">
            <h1>Sign Up</h1>
          </div>

          <div className="app-login-form">
            <Formik
              initialValues={{ email: email, password: password, name: name }}
              onSubmit={handleClickSubmit}
              validationSchema={Yup.object().shape({
                name: Yup.string().required("Please enter Name"),
                email: Yup.string()
                  .required("Please enter email")
                  .email("Please enter valid email"),
                password: Yup.string().required("Please enter password"),
              })}
            >
              {(props) => {
                const {
                  values,
                  touched,
                  errors,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                } = props;
                return (
                  <form noValidate onSubmit={handleSubmit}>
                    <fieldset>
                      <TextField
                        autoFocus
                        value={values.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        variant="outlined"
                        name="name"
                        label="Name"
                        fullWidth
                        error={errors.name && touched.name}
                        helperText={errors.name && touched.name && errors.name}
                        margin="dense"
                        className="mt-1 my-sm-3"
                      />
                      <TextField
                        autoFocus
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        variant="outlined"
                        name="email"
                        label="Email"
                        fullWidth
                        error={errors.email && touched.email}
                        helperText={
                          errors.email && touched.email && errors.email
                        }
                        margin="dense"
                        className="mt-1 my-sm-3"
                      />
                      <TextField
                        variant="outlined"
                        type="password"
                        value={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="password"
                        label="Password"
                        fullWidth
                        error={errors.password && touched.password}
                        helperText={
                          errors.password && touched.password && errors.password
                        }
                        margin="dense"
                        className="mt-1 my-sm-3"
                      />

                      <div className="my-3 d-flex align-items-center justify-content-between">
                        <Button
                          fullWidth
                          type="submit"
                          variant="contained"
                          color="primary"
                        >
                          Register
                        </Button>

                        <Button
                          fullWidth
                          variant="contained"
                          onClick={handleRegister}
                          className="ml-3"
                          color="primary"
                        >
                          Sign In
                        </Button>
                      </div>
                    </fieldset>
                  </form>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignUp;
