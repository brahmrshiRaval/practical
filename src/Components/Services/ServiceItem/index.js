import React from "react";
import { Card, CardBody, CardTitle, CardSubtitle, Button } from "reactstrap";
import { CheckCircle } from "@material-ui/icons";

const ServiceCard = (props) => {
  return (
    <Card
      className="shadow service-card "
      style={{ overflow: "hidden", height: "100% " }}
    >
      <CardBody className="text-center">
        <CardTitle className="font-weight-bold" style={{ fontSize: "18px" }}>
          {props.item.Name}
        </CardTitle>
        <CardSubtitle>
          <h5 style={{ fontSize: "1.50rem", display: "inline-flex" }}>
            {" "}
            &#x20b9;
            {props.item.price}
          </h5>
        </CardSubtitle>
        <div
          style={{ maxHeight: "70px", minHeight: "70px", marginBottom: "12x" }}
          className=""
        >
          <p style={{ fontSize: "1.15rem" }}>{props.item.Location}</p>
          <p>Quntity :{props.item.qty}</p>
        </div>
        <div>
          <div style={{ marginTop: "7px" }}>
            {props.isInCart ? (
              <Button type="button" color="danger" className="theme-font">
                <CheckCircle fontSize="small" /> Added
              </Button>
            ) : (
              <Button
                outline
                type="button"
                color="danger"
                className="theme-font "
                onClick={() => {
                  props.handleAddcart(props.item, false);
                }}
              >
                ADD TO CART
              </Button>
            )}
          </div>
        </div>
      </CardBody>
    </Card>
  );
};

export default ServiceCard;
