import React from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  Button,
  CircularProgress,
  DialogActions,
} from "@material-ui/core";

export default class ConfirmDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: this.props.isVisible || false,
      isLoading: this.props.isLoading || false,
      title: this.props.title || "Are you sure?",
      description:
        this.props.description || "Do you want to delete this record?",
    };
  }

  handleRequestClose = (is) => {
    this.props.onSubmit(is);
  };

  render() {
    return (
      <Dialog
        disableBackdropClick
        open={this.state.open}
        onClose={() => this.handleRequestClose(false)}
      >
        {this.state.isLoading && (
          <div className="model-loader-view">
            <CircularProgress />
          </div>
        )}
        <DialogTitle>{this.state.title}</DialogTitle>
        <DialogContent>
          <DialogContentText>{this.state.description}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => this.handleRequestClose(false)}
            color="secondary"
          >
            No
          </Button>
          <Button onClick={() => this.handleRequestClose(true)} color="primary">
            Yes, confirm.
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}
