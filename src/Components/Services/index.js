import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Button from "@material-ui/core/Button";
import { userSignOut } from "../../actiions/Auth";

const Signout = (props) => {
  const dispatch = useDispatch();
  debugger;
  const handleClickSubmit = () => {
    dispatch(userSignOut());
  };
  debugger;
  const token = useSelector((Auth) => Auth.form.Auth.token);
  useEffect(() => {
    debugger;
    if (!token) {
      props.history.push("/");
    }
  }, [token, props.history]);
  return (
    <div className="app-login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
      <div className="app-login-main-content">
        <div className="app-login-content">
          <div className="app-login-header mb-4 text-center">
            <h1>Admin Login</h1>
          </div>

          <div className="app-login-form">
            <Button
              fullWidth
              variant="contained"
              onClick={handleClickSubmit}
              color="primary"
            >
              Sign Out
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Signout;
