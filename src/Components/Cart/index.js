import React, { Component } from "react";
import Cart from "./Cart";
import { connect } from "react-redux";
class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: "cart",
      cart: [],
      subtotal: 0,
      total: 0,
      cartdatareducer: this.props.cart,
    };
  }
  componentWillMount = () => {
    this.handlesutotal();
  };
  componentWillReceiveProps(nextProps) {
    debugger;
    this.setState({
      cartdatareducer: nextProps.cart,
    });
    this.handlesutotal();
  }
  handleTotal = (sub, dis) => {
    debugger;
    let total = sub;
    this.setState({
      total: total,
    });
  };
  handlesutotal = () => {
    let sub = 0;
    this.state.cartdatareducer.map((item) => {
      debugger;
      let mainPrice = item.price;
      return [(sub = sub + mainPrice)];
    });
    this.setState({
      subtotal: sub,
    });
    this.handleTotal(sub, 0);
  };
  onDeleteItem = (serviceName) => {
    var cart = this.state.cartdatareducer;
    const currentIndex = cart.findIndex(function (single) {
      var singledata = single.Name;
      return singledata === serviceName;
    });
    if (currentIndex !== -1) {
      cart.splice(currentIndex, 1);
      this.setState({ cartdatareducer: cart }, () => {
        this.handlesutotal();
        this.props.deleteCartdata(this.state.cartdatareducer);
      });
    }
  };
  render() {
    const { step, subtotal, disscount, total } = this.state;
    switch (step) {
      case "cart":
        return (
          <Cart
            cartdata={this.state.cartdatareducer}
            onClickDelete={(name) => {
              this.onDeleteItem(name);
            }}
            subtotalamount={subtotal}
            disscountapply={this.state.disscountapply}
            disscountdetails={this.state.disscountdetails}
            disscountamount={disscount}
            totalamount={total}
            handleCloseDisscount={() => {
              this.handleCloseDisscount();
            }}
            handleDisscount={(data) => {
              this.handleDisscount(data);
            }}
            handeNextStep={(data) => {
              this.handeNextStep(data);
            }}
          />
        );
      default:
        return (
          <Cart
            cartdata={this.state.cartdatareducer}
            onClickDelete={(name) => {
              this.onDeleteItem(name);
            }}
            subtotalamount={subtotal}
            disscountamount={disscount}
            totalamount={total}
            handeNextStep={(data) => {
              this.handeNextStep(data);
            }}
          />
        );
    }
  }
}
const mapStateToProps = (state) => {
  debugger;
  return {
    cart: state.form.Cart.cart,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    deleteCartdata: (data) => dispatch({ type: "DELETE_ITEM", payload: data }),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(index);
