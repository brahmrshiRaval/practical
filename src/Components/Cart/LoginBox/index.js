import React from "react";
import {
  Dialog,
  // DialogTitle,
  DialogContent,
  // DialogContentText,
  // Button,
  // CircularProgress,
  // DialogActions,
} from "@material-ui/core";

import LoginStep from "./Loginform/index";

export default class ConfirmDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: this.props.isVisible || false,
    };
  }
  closeDilog = () => {
    this.props.closeDilog();
  };
  handleRequestClose = (is) => {
    this.props.onSubmit(is);
  };

  render() {
    return (
      <Dialog
        disableBackdropClick
        open={this.state.open}
        // onClose={() => this.handleRequestClose(false)}
        className="dilog-width"
      >
        {/* <DialogTitle></DialogTitle> */}
        <DialogContent>
          <LoginStep
            handleRequestClose={(DATA) => {
              this.handleRequestClose(false);
            }}
            closeDilog={() => {
              this.closeDilog();
            }}
          ></LoginStep>
        </DialogContent>
      </Dialog>
    );
  }
}
