import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import OtpInput from "react-otp-input";
import base64 from "base-64";
import { setSession } from "../../../../utils/Session";
import { Button } from "@material-ui/core";
import { connect } from "react-redux";
import { Tooltip, IconButton } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/CancelOutlined";
class otpVerificationform extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobile: this.props.mobilenumber,
      otp: "",
      errors: {
        otp: "",
        incorrect: "",
      },
      userData: this.props.token,
    };
  }
  componentWillReceiveProps(nextProps) {
    debugger;
    this.setState({
      userData: nextProps.token,
    });
  }
  handelVerify = () => {
    debugger;
    let validation = this.validation();
    if (validation) {
      var encoded = base64.encode(this.state.otp);
      var otp = this.state.userData.otp;
      var data = this.state.userData.user;
      if (encoded === otp) {
        if (data.user_name === null) {
          setSession(this.state.userData);
          this.props.onhandelClick();
        } else {
          setSession(this.state.userData);
          this.props.otpVerify();
          this.props.otpVerification();
          this.props.onhandelClick();
        }
      } else {
        this.setState({
          errors: {
            incorrect: "Incorrect Otp",
          },
        });
      }
    }
  };
  handleeditnumber = () => {
    this.props.onEditClik();
  };
  validation = () => {
    let error = this.state.errors;
    let formIsValid = true;
    if (!this.state.otp) {
      formIsValid = false;
      error.otp = "Please Enter OTP.";
    }
    if (this.state.otp.length !== 4) {
      formIsValid = false;
      error.otp = "Please Enter OTP.";
    }

    this.setState({
      errors: error,
    });
    return formIsValid;
  };
  render() {
    return (
      <div className="OTP-form-box text-center">
        <div className="text-right closebutton">
          <Tooltip title={"Close"}>
            <IconButton
              color="primary"
              className="p-0"
              onClick={() => {
                this.props.closeDilog();
              }}
            >
              <CloseIcon />
            </IconButton>
          </Tooltip>
        </div>
        <h2 className="mb-20">OTP Verification</h2>
        <p className="mb-20" style={{ fontSize: "14px" }}>
          We have sent your OTP on your
          <br />
          mobile no: +91 {this.state.mobile}
          <b className="resend-option" onClick={this.handleeditnumber}>
            {" "}
            EDIT
          </b>
        </p>
        <Row className="">
          <Col className="otp-input-style p-0 text-center m-auto">
            <OtpInput
              value={this.state.otp}
              onChange={(e) => {
                this.setState({ otp: e });
              }}
              numInputs={4}
              hasErrored={true}
              separator={<span> </span>}
            />
            {this.state.errors.otp ? (
              <div
                className="errorMsg text-left"
                style={{
                  fontSize: "12px",
                  color: "red",
                  fontweight: "600",
                  paddingTop: "10px",
                  paddingLeft: "20px",
                }}
              >
                {this.state.errors.otp}
              </div>
            ) : null}
            {this.state.errors.incorrect ? (
              <div
                className="errorMsg"
                style={{
                  fontSize: "12px",
                  color: "red",
                  fontweight: "600",
                  padding: "5x 0 10px",
                }}
              >
                {this.state.errors.incorrect}
              </div>
            ) : null}
          </Col>
        </Row>
        <p className="resend-option mb-20 mt-20">
          <b>
            <u>Send Again ?</u>
          </b>
        </p>
        <div className="button-style mt-20 text-center">
          <Button onClick={this.handelVerify}>Verify</Button>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state, response) => {
  debugger;
  return {
    token: state.form.Auth.authUser === null ? "" : state.form.Auth.authUser,
  };
};
const mapDispatchToProps = (dispatch) => {
  debugger;
  return {
    // dispatching plain actions
    otpVerify: (data) => dispatch({ type: "USER_OTP_VERIFY" }),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(otpVerificationform);
