import React, { Component } from "react";
import { TextField, Button } from "@material-ui/core";
import { Formik } from "formik";
import * as Yup from "yup";
// import FormControlLabel from "@material-ui/core/FormControlLabel";
import ApiRequests from "../../../../utils/Request";
import Checkbox from "@material-ui/core/Checkbox";
import { connect } from "react-redux";
import { Tooltip, IconButton } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/CancelOutlined";
class submitForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data || {
        user_name: "",
        user_email: "",
        user_mobile: "",
      },
      errors: {
        email: "",
        agree: "",
      },
      checked: false,
    };
  }
  handleClickSubmit = (values) => {
    debugger;
    let validation = this.validation();
    const { user_email, user_name } = values;
    debugger;
    var formdata = new FormData();
    formdata.append("user_email", user_email);
    formdata.append("user_name", user_name);
    formdata.append("remove_current_image", "0");
    if (validation) {
      ApiRequests.updateProfile(formdata)
        .then((response) => {
          debugger;
          console.log(response);
          if (response.data.status === 1) {
            this.props.otpVerify();
            this.props.formSubmit();
          }
          if (response.data.state === 2) {
            debugger;
            this.setState({
              errors: {
                email: "User Email Is Already Exists",
              },
            });
          }
        })
        .catch((err) => {
          debugger;
          console.log(err);
        });
    }
  };
  handleChecked = () => {
    debugger;
    this.setState({
      checked: !this.state.checked,
    });
  };
  validation = () => {
    let error = this.state.errors;
    let formIsValid = true;
    debugger;
    if (this.state.checked === false) {
      formIsValid = false;
      error.agree = "Requird";
    } else {
      error.agree = "";
    }
    this.setState({
      errors: error,
    });
    return formIsValid;
  };
  render() {
    return (
      <div className="final-form-box text-center">
        <div className="text-right closebutton">
          <Tooltip title={"Close"}>
            <IconButton
              color="primary"
              className="p-0"
              onClick={() => {
                this.props.closeDilog();
              }}
            >
              <CloseIcon />
            </IconButton>
          </Tooltip>
        </div>
        <h4 className="mb-20">Sign Up</h4>
        <Formik
          initialValues={{ ...this.state.data }}
          onSubmit={this.handleClickSubmit}
          validationSchema={Yup.object().shape({
            user_name: Yup.string().nullable().required("Name Required"),
            user_mobile: Yup.string()
              .matches(/^[0-9]+$/, "Must be only digits")
              .max(10, "Must be 10 Digit")
              .min(10, "Must be 10 Digit")
              .required("Required"),
            user_email: Yup.string()
              .nullable()
              .email("Invalid email")
              .required("Email Required"),
          })}
        >
          {(props) => {
            const {
              values,
              // touched,
              errors,
              // dirty,
              handleChange,
              handleBlur,
              handleSubmit,
              // setFieldValue,
              // handleReset,
            } = props;
            return (
              <form noValidate onSubmit={handleSubmit}>
                <div className="mb-20">
                  <TextField
                    value={values.user_name}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    required
                    name="user_name"
                    type="text"
                    label="Your Name"
                    variant="outlined"
                    className="texfiled-width"
                    // error={errors.user_name && touched.user_name}
                    // helperText={
                    // errors.user_name && touched.user_name && errors.user_name
                    // }
                  />
                  {errors.user_name ? (
                    <div
                      className="errorMsg text-left"
                      style={{
                        fontSize: "12px",
                        color: "red",
                        fontweight: "600",
                        padding: "10px 0 0",
                      }}
                    >
                      {errors.user_name}
                    </div>
                  ) : null}
                </div>
                <div className="mb-20">
                  <TextField
                    value={values.user_email}
                    name="user_email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    required
                    label="Your Email"
                    type="email"
                    variant="outlined"
                    className="texfiled-width"
                    // error={errors.user_email && touched.user_email}
                    // helperText={
                    //   errors.user_email && touched.user_email && errors.user_email
                    // }
                  />
                  {errors.user_email ? (
                    <div
                      className="errorMsg text-left"
                      style={{
                        fontSize: "12px",
                        color: "red",
                        fontweight: "600",
                        padding: "10px 0 0",
                      }}
                    >
                      {errors.user_email}
                    </div>
                  ) : null}
                  {this.state.errors.email ? (
                    <div
                      className="errorMsg text-left"
                      style={{
                        fontSize: "12px",
                        color: "red",
                        fontweight: "600",
                        padding: "10px 0 0",
                      }}
                    >
                      {this.state.errors.email}
                    </div>
                  ) : null}
                </div>
                <div className="mb-20">
                  <TextField
                    value={values.user_mobile}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    required
                    name="user_mobile"
                    label="Your Number"
                    type="text"
                    variant="outlined"
                    disabled={true}
                    className="texfiled-width"
                    // error={errors.user_mobile && touched.user_mobile}
                    // helperText={
                    //   errors.user_mobile &&
                    //   touched.user_mobile &&
                    //   errors.user_mobile
                    // }
                  />
                  {errors.user_mobile ? (
                    <div
                      className="errorMsg text-left"
                      style={{
                        fontSize: "12px",
                        color: "red",
                        fontweight: "600",
                        padding: "10px 0 0",
                      }}
                    >
                      {errors.user_mobile}
                    </div>
                  ) : null}
                </div>
                <div className="checkbox-style">
                  <div>
                    <Checkbox
                      name="agree"
                      value={this.state.checked}
                      onChange={() => this.handleChecked()}
                      inputProps={{ "aria-label": "checkbox with small size" }}
                      label="Agree Termes And Condition"
                    />
                  </div>
                  <div>
                    <p className="ml-2">Agree Termes And Condition</p>
                  </div>
                </div>
                {this.state.errors.agree ? (
                  <div
                    className="mt-n3"
                    style={{
                      fontSize: "12px",
                      color: "red",
                      fontweight: "600",
                      padding: "10px 0 0",
                    }}
                  >
                    {this.state.errors.agree}
                  </div>
                ) : null}
                <div className="button-style mt-10 text-center">
                  <Button type="submit">Save</Button>
                </div>
              </form>
            );
          }}
        </Formik>
      </div>
    );
  }
}
const mapStateToProps = (state) => {};

const mapDispatchToProps = (dispatch) => {
  debugger;
  return {
    // dispatching plain actions
    otpVerify: () => dispatch({ type: "USER_OTP_VERIFY" }),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(submitForm);
