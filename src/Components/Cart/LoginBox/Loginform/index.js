import React, { Component } from "react";
// import axios from "axios";
// import base64 from "base-64";
import { withRouter } from "react-router-dom";
// import { AppSettings } from "../../../utils/AppSetting";
// import { getSession } from "../../../utils/Session";
import { userSignIn } from "../../../../actiions/index";
import { connect } from "react-redux";
import Loginform from "./loginform";
import OtpForm from "./OtpVerification";
import "./login.scss";

import SubmitForm from "./Submit";

class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      user_mobile: "",
      otp: "",
      name: "",
      emial: "",
      data: {
        user_name: "",
        user_email: "",
        user_mobile: "",
      },
      rowdata: [],
    };
  }
  handeNextStep = () => {
    debugger;
    this.setState({
      step: this.state.step + 1,
    });
  };
  handeEditNumber = () => {
    this.setState({
      step: this.state.step - 1,
    });
  };
  getotp = (value) => {
    debugger;
    this.setState({
      user_mobile: value,
      data: {
        user_mobile: value,
      },
    });

    var formdata = new FormData();
    formdata.append("user_mobile", value);
    let data = this.props.userSignIn(value);
    console.log(data);
  };
  otpVerification = () => {
    debugger;
    this.props.handleRequestClose(false);
    // window.location.href = "/profile";
  };
  formSubmit = () => {
    debugger;
    this.props.handleRequestClose(false);
    // window.location.href = "/profile";
  };
  closeDilog = () => {
    this.props.closeDilog();
  };
  render() {
    const { step } = this.state;
    switch (step) {
      case 1:
        return (
          <Loginform
            onhandelClick={this.handeNextStep}
            getmobileno={(value) => this.getotp(value)}
            mobilenumber={this.state.user_mobile}
            closeDilog={() => {
              this.closeDilog();
            }}
          />
        );
      case 2:
        return (
          <OtpForm
            onhandelClick={this.handeNextStep}
            onEditClik={this.handeEditNumber}
            mobilenumber={this.state.user_mobile}
            otpvalue={this.state.otp}
            otpVerification={() => this.otpVerification()}
            closeDilog={() => {
              this.closeDilog();
            }}
          />
        );
      case 3:
        return (
          <SubmitForm
            onhandelClick={this.handeNextStep}
            data={this.state.data}
            formSubmit={() => this.formSubmit()}
            closeDilog={() => {
              this.closeDilog();
            }}
          />
        );
      default:
        return (
          <Loginform
            onhandelClick={this.handeNextStep}
            getmobileno={(value) => this.getotp(value)}
            mobilenumber={this.state.user_mobile}
            closeDilog={() => {
              this.closeDilog();
            }}
          />
        );
    }
  }
}
const mapStateToProps = (state, response) => {
  return {
    response,
  };
};
const mapDispatchToProps = {
  userSignIn: userSignIn,
};
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(index));
