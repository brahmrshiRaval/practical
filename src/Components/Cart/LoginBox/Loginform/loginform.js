import React, { Component } from "react";
import { TextField, Button } from "@material-ui/core";
import { Formik } from "formik";
import * as Yup from "yup";
import { Tooltip, IconButton } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/CancelOutlined";
export default class lginform extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobile: this.props.mobilenumber
        ? { mobile: this.props.mobilenumber }
        : { mobile: "" },
    };
  }
  handeleGetOtp = (values) => {
    debugger;
    this.props.getmobileno(values.mobile);
    this.props.onhandelClick();
  };
  render() {
    return (
      <div className="login-form-dilog">
        {/* <div className="d-inline-flex"> */}
        <div className="text-right closebutton">
          <Tooltip title={"Close"}>
            <IconButton
              color="primary"
              className="p-0"
              onClick={() => {
                this.props.closeDilog();
              }}
            >
              <CloseIcon />
            </IconButton>
          </Tooltip>
        </div>
        <h2 className="mb-20 text-center">Login</h2>
        {/* </div> */}
        <Formik
          initialValues={{ ...this.state.mobile }}
          onSubmit={this.handeleGetOtp}
          validationSchema={Yup.object().shape({
            mobile: Yup.string()
              .matches(/^[0-9]+$/, "Must be only digits")
              .max(10, "Must be 10 Digit")
              .min(10, "Must be 10 Digit")
              .required("Please Enter your Mobile number"),
            // mobile: Yup.string().required('age is required').test(10, 'Must be exactly 10 characters', val => val.toString().length === 10),
            // .matches(/^[0-9]+$/, "Must be only digits"),
          })}
        >
          {(props) => {
            const {
              values,
              // touched,
              errors,
              // dirty,
              handleChange,
              handleBlur,
              handleSubmit,
              // setFieldValue,
              // handleReset,
            } = props;
            return (
              <form noValidate onSubmit={handleSubmit}>
                <TextField
                  value={values.mobile}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="mobile"
                  type="number"
                  variant="outlined"
                  label="Mobile Number"
                  required
                  rows={3}
                  className="texfiled-width"
                  onInput={(e) => {
                    e.target.value = Math.max(0, parseInt(e.target.value))
                      .toString()
                      .slice(0, 10);
                  }}
                  min={0}
                  // error={errors.mobile && touched.mobile}
                  // helperText={errors.mobile && touched.mobile && errors.mobile}
                />
                {errors.mobile ? (
                  <div
                    className="errorMsg"
                    style={{
                      fontSize: "12px",
                      color: "red",
                      fontweight: "600",
                      padding: "10px 0 0",
                    }}
                  >
                    {errors.mobile}
                  </div>
                ) : null}
                <div className="button-style mt-20 text-center">
                  <Button type="submit">Get OTP</Button>
                </div>
              </form>
            );
          }}
        </Formik>
      </div>
    );
  }
}
