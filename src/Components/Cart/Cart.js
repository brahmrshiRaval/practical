import React, { Component, Fragment } from "react";
// import money from "../../Images/money.svg";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardHeader,
  Button,
} from "reactstrap";
import { connect } from "react-redux";
import { Tooltip } from "@material-ui/core";
import Header from "../Header";
import DeleteIcon from "@material-ui/icons/Delete";

class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // cartlist: this.props.cartdata,
      subtitotal: this.props.subtotalamount,
      total: this.props.totalamount,
      cart: this.props.cartdata,
      isModel: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      subtitotal: nextProps.subtotalamount,
      total: nextProps.totalamount,
    });
  }
  HandleClose = () => {
    this.setState({
      isModel: false,
    });
    this.props.handeNextStep("adressoicker");
  };
  closeDilog = () => {
    this.setState({
      isModel: false,
    });
  };
  handeNextStep = () => {
    debugger;
    this.props.handeNextStep("adressoicker");
  };

  render() {
    debugger;
    const { cart, subtitotal, total } = this.state;
    return (
      <Fragment>
        <Header />
        <Container>
          <div className="cart-style">
            <Card className="card-style">
              <CardHeader>
                <h2>Cart</h2>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col className="item-list-style">
                    <Row className="service-header">
                      <Col md={6} sm={6} xs={6} className="p-0">
                        <p>Services items</p>
                      </Col>
                      <Col
                        md={6}
                        sm={6}
                        xs={6}
                        className="p-0 text-righ"
                        style={{ textAlign: "end" }}
                      >
                        <p>PRICES</p>
                      </Col>
                    </Row>
                    {cart.map((item, index) => {
                      return (
                        <Row className="itemlist-style" key={index}>
                          <Col md={6} sm={6} xs={6} className="p-0 d-flex ">
                            <p>{item.Name}</p>
                          </Col>
                          <Col
                            md={6}
                            sm={6}
                            xs={6}
                            className="p-0 text-right d-flex item-name"
                          >
                            <Tooltip title={"Delete"}>
                              <DeleteIcon
                                fontSize={"inherit"}
                                onClick={() => {
                                  this.props.onClickDelete(item.Name);
                                }}
                              />
                            </Tooltip>
                            <p>
                              &#x20b9;
                              <b>{item.price}</b>
                            </p>
                          </Col>
                        </Row>
                      );
                    })}
                  </Col>
                </Row>

                <Row>
                  <Col>
                    <Row>
                      <Col className="grandtotla-style">
                        <Row className="itemlist-style">
                          <Col md={6} sm={6} xs={6} className="p-0">
                            <p>SUB TOTAL</p>
                          </Col>
                          <Col md={6} sm={6} xs={6} className="p-0 text-right">
                            <p className="money">
                              &#x20b9;
                              <b>{parseFloat(subtitotal).toFixed(2)}</b>
                            </p>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Row>
                      <Col className="total-style">
                        <Row className="itemlist-style">
                          <Col md={6} sm={6} xs={6} className="p-0">
                            <p>Total</p>
                          </Col>
                          <Col md={6} sm={6} xs={6} className="p-0 text-right">
                            <p>
                              &#x20b9;
                              <b>{parseFloat(total).toFixed(2)}</b>
                            </p>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </div>
        </Container>
      </Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  car: state.form.Cart.car,
  token:
    state.form.Auth.authUser === null ? false : state.form.Auth.isOTPVerify,
  cart: state.form.Cart?.cart || [],
});

export default connect(mapStateToProps)(Cart);
