import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { userSignIn } from "../../actiions/Auth";
import { Formik } from "formik";
import * as Yup from "yup";

const SignIn = (props) => {
  const [email] = useState();
  const [password] = useState();
  const dispatch = useDispatch();
  const user = useSelector((Auth) => Auth.form.Auth.userData);
  debugger;
  const token = useSelector((Auth) => Auth.form.Auth.token);
  useEffect(() => {
    debugger;
    if (token) {
      console.log(props.history);
      props.history.push("/service");
    }
  }, [token, props.history]);

  const handleClickSubmit = (values) => {
    debugger;
    const currentIndex = user.findIndex(function (single) {
      return single.email === values.email;
    });
    let usermailretrive = user[currentIndex].email;
    let userpasswordretrive = user[currentIndex].password;
    if (
      usermailretrive === values.email &&
      userpasswordretrive === values.password
    ) {
      dispatch(userSignIn({ ...values }));
    }

    // dispatch(userSignIn({ ...values }));
  };
  const handleRegister = () => {
    props.history.push("/register");
  };
  return (
    <div className="app-login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
      <div className="app-login-main-content">
        <div className="app-login-content">
          <div className="app-login-header mb-4 text-center">
            <h1>Login to your account</h1>
          </div>

          <div className="app-login-form">
            <Formik
              initialValues={{ email: email, password: password }}
              onSubmit={handleClickSubmit}
              validationSchema={Yup.object().shape({
                email: Yup.string()
                  .required("Please enter email")
                  .email("Please enter valid email"),
                password: Yup.string().required("Please enter password"),
              })}
            >
              {(props) => {
                const {
                  values,
                  touched,
                  errors,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                } = props;
                return (
                  <form noValidate onSubmit={handleSubmit}>
                    <fieldset>
                      <TextField
                        autoFocus
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        variant="outlined"
                        name="email"
                        label="Email"
                        fullWidth
                        error={errors.email && touched.email}
                        helperText={
                          errors.email && touched.email && errors.email
                        }
                        margin="dense"
                        className="mt-1 my-sm-3"
                      />
                      <TextField
                        variant="outlined"
                        type="password"
                        value={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="password"
                        label="Password"
                        fullWidth
                        error={errors.password && touched.password}
                        helperText={
                          errors.password && touched.password && errors.password
                        }
                        margin="dense"
                        className="mt-1 my-sm-3"
                      />

                      <div className="my-3 d-flex align-items-center justify-content-between">
                        <Button
                          fullWidth
                          type="submit"
                          variant="contained"
                          color="primary"
                        >
                          Sign In
                        </Button>
                        <Button
                          fullWidth
                          variant="contained"
                          onClick={handleRegister}
                          className="ml-3"
                          color="primary"
                        >
                          Register
                        </Button>
                      </div>
                    </fieldset>
                  </form>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignIn;
